import React, { Component } from 'react';
import * as d3 from d3

class Testd3 extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  componentDidMount() {
    let accessToRef = d3.select(this.myRef.current);
    accessToRef.style("background-color", "green")
  }

  render() {
    return (
      <div ref={this.myRef}>Testing D3 Refs</div>
    );
  }

}

export default Testd3;